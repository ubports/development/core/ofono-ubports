Source: ofono-ubports
Section: comm
Priority: optional
Maintainer: UBports developers <devs@ubports.com>
Build-Depends: debhelper-compat (= 12),
               libbluetooth-dev (>= 4.30),
               libc-ares-dev,
               libdbus-1-dev,
               libdbus-glib-1-dev,
               libglib2.0-dev,
               libsystemd-dev,
               libtool,
               libudev-dev,
               mobile-broadband-provider-info,
               udev,
Standards-Version: 3.9.4
Homepage: https://gitlab.com/ubports/core/ofono-ubports
Vcs-Git: https://gitlab.com/ubports/core/ofono-ubports.git
Vcs-Browser: https://gitlab.com/ubports/core/ofono-ubports

Package: ofono-ubports
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, dbus
Recommends: udev
Conflicts: ofono, ofono-sailfish
Provides: ofono
Description: Mobile telephony stack (daemon)
 oFono is a stack for mobile telephony devices on Linux and this package
 contains a fork of version 1.18 by the UBports project with support added for
 Android's RIL interface over a UNIX socket which only works with Android 4 to
 7.  oFono supports speaking to telephony devices through specific drivers, or
 with generic AT commands.
 .
 oFono also includes a low-level plug-in API for integrating with other
 telephony stacks, cellular modems and storage back-ends. The plug-in API
 functionality is modeled on public standards, in particular
 3GPP TS 27.007 "AT command set for User Equipment (UE)."
 .
 This package includes the core daemon.

Package: ofono-ubports-dev
Section: libdevel
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libdbus-1-dev,
         libglib2.0-dev,
         libc6-dev | libc-dev
Description: Mobile telephony stack (development files)
 oFono is a stack for mobile telephony devices on Linux and this package
 contains a fork of version 1.18 by the UBports project with support added for
 Android's RIL interface over a UNIX socket which only works with Android 4 to
 7.  oFono supports speaking to telephony devices through specific drivers, or
 with generic AT commands.
 .
 oFono also includes a low-level plug-in API for integrating with other
 telephony stacks, cellular modems and storage back-ends. The plug-in API
 functionality is modeled on public standards, in particular
 3GPP TS 27.007 "AT command set for User Equipment (UE)."
 .
 This package includes the header files for building oFono plugins.

Package: ofono-ubports-scripts
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ofono-ubports (>= ${source:Version}),
         python3,
         python3-dbus,
         python3-gi
Conflicts: ofono-scripts, ofono-sailfish-scripts
Description: Mobile telephony stack (test and maintenance script files)
 oFono is a stack for mobile telephony devices on Linux and this package
 contains a fork of version 1.18 by the UBports project with support added for
 Android's RIL interface over a UNIX socket which only works with Android 4 to
 7.  oFono supports speaking to telephony devices through specific drivers, or
 with generic AT commands.
 .
 oFono also includes a low-level plug-in API for integrating with other
 telephony stacks, cellular modems and storage back-ends. The plug-in API
 functionality is modeled on public standards, in particular
 3GPP TS 27.007 "AT command set for User Equipment (UE)."
 .
 This package includes test and maintenance scripts.
